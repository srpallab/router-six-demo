import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Navigate,
    Link,
    Outlet,
    useParams,
    NavLink,
    useNavigate,
    useLocation,
} from 'react-router-dom';


ReactDOM.render(
  <Router>
    <Routes>
      <Route path="/" element={<Home />}/>
      <Route path="learn" element={<Learn />} >
        <Route path='courses' element={<Courses />}>
          <Route path=":courseslug" element={<Course />}/>
        </Route>
        <Route path='bundle' element={<Bundle />} />
      </Route>
      <Route path='coursedashboard' element={<CourseDashboard />}/>
      <Route path="profile" element={<Navigate replace to='/login' />}/>
      <Route path="login" element={<Login />}/>
    </Routes>
  </Router>,
  document.getElementById('root')
);


function Home(){
    return(
        <div>
          <h1>Home Route.</h1>
          <Link to='/learn'>Go To Learn Page</Link>
        </div>
    );
}

function Learn(){
    return(
        <div>
          <h1>Learn Page.</h1>
          <Link to='/learn/courses'> Go To All Courses </Link> |
          <Link to='/learn/bundle'> Go To All Bundle </Link>
          <Outlet />
        </div>
    );
}

function Courses(){
    const coursesList = ['React', 'Express', 'MongoDB', 'Vue'];
    return(
        <div>
          <h1>Courses Page</h1>
          {
              coursesList.map((course) => {
                  return(
                      <NavLink key={course} to={`/learn/courses/${course}`}>
                        <h4>{course} Card</h4>
                      </NavLink>
                  );
              })
          }
          <Outlet />
        </div>
    );
}

function Course(){
    const {courseslug} = useParams();
    const navigate = useNavigate();
    return(
        <div>
          <h1>Course Details</h1>
          <h4>Courese Slug Is: {courseslug}</h4>
          <button
            onClick={() => {
                navigate('/coursedashboard', {state: `${courseslug}`});
            }}>
            Learn More
          </button>
          <Link to='/coursedashboard' state={'DJANGO'}>Buy Now</Link>
        </div>
    );
}

function CourseDashboard(){
    const location = useLocation();
    
    return(
        <div>
          <h1>Details About {location.state}</h1>
        </div>
    );
}

function Bundle(){
    return(
        <div>
          <h1>Bundle Page.</h1>
          <h4>Bundle Cards</h4>
        </div>
    );
}


function Login(){
    return(
        <div>
          <h1>Login Page.</h1>
        </div>
    );
}


reportWebVitals();
